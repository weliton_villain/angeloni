import Sequelize from 'sequelize';

import Cell from '../app/models/Cell';
import Profile from '../app/models/Profile';
import User from '../app/models/User';
import Management from '../app/models/Management';
import Area from '../app/models/Area';

import databaseConfig from '../config/database';

const models = [User, Profile, Cell, Management, Area];

class Database {
  constructor() {
    this.init();
  }

  init() {
    this.connection = new Sequelize(databaseConfig);

    models
      .map(model => model.init(this.connection))
      .map(model => model.associate && model.associate(this.connection.models));
  }
}

export default new Database();
