import Sequelize, { Model } from 'sequelize';

class Management extends Model {
  static init(sequelize) {
    super.init(
      {
        code: Sequelize.INTEGER,
        name: Sequelize.STRING,
      },
      {
        sequelize,
      }
    );
  }
}

export default Management;
