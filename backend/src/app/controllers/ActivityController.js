import Activity from '../models/Activity';
import Project from '../models/Project';

class ActivityController {
  async store(req, res) {
    const { activity, classification, date, project_id } = await req.body;

    const project = await Project.findOne({ where: { id: project_id } });

    if (!project) {
      return res.status(400).json({ error: 'Project does not match' });
    }

    const act = await Activity.create({
      activity,
      classification,
      date,
      user_id: req.userId,
      project_id: project.id,
    });

    return res.json(act);
  }

  async index(req, res) {
    const activities = await Activity.findAll({
      attributes: [
        'activity',
        'classification',
        'date',
        'user_id',
        'project_id',
      ],
    });

    return res.json(activities);
  }
}

export default new ActivityController();
