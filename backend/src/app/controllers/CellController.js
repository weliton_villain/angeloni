import Cell from '../models/Cell';

class CellController {
  async store(req, res) {
    const cell = await Cell.findOne({ where: { code: req.body.code } });

    if (cell) {
      return res.status(401).json({ error: 'Cell already exists.' });
    }

    const { id, code, name } = await Cell.create(req.body);

    return res.json({
      id,
      code,
      name,
    });
  }
}

export default new CellController();
