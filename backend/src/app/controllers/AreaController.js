import Area from '../models/Area';

class AreaController {
  async store(req, res) {
    const { id, code, name } = req.body;

    const area = await Area.findOne({ where: { code } });

    if (area) {
      return res.status(401).json({ error: 'Area already exists' });
    }

    await Area.create({
      id,
      code,
      name,
      // management_id: Area.addManagement(req.body.mgt_code),
    });

    return res.json({
      id,
      code,
      name,
    });
  }
}

export default new AreaController();
