import Management from '../models/Management';

class ManagementController {
  async store(req, res) {
    const management = await Management.findOne({
      where: { code: req.body.code },
    });

    if (management) {
      return res.status(401).json({ error: 'Management already exists' });
    }

    const { id, code, name } = await Management.create(management);

    return res.json({
      id,
      code,
      name,
    });
  }
}

export default new ManagementController();
