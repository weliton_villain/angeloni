import Project from '../models/Project';

class ProjectController {
  async store(req, res) {
    const project = await Project.findOne({ where: { code: req.body.code } });

    if (project) {
      return res.status(400).json({ error: 'Project already exists' });
    }

    const { id, code, title } = await Project.create(req.body);

    return res.json({
      id,
      code,
      title,
    });
  }

  async index(req, res) {
    const projects = await Project.findAll({
      attributes: ['id', 'code', 'title'],
    });

    return res.json(projects);
  }
}

export default new ProjectController();
