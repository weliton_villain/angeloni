import Solicitation from '../models/Solicitation';

class SolicitationController {
  async store(req, res) {
    const solicitation = await Solicitation.findOne({
      where: { code: req.body.code },
    });

    if (solicitation) {
      return res.status(400).json({ error: 'Solicitation already exists' });
    }

    const { id, code, title, user_id, project_id } = await Solicitation.create(
      req.body
    );

    return res.json({
      id,
      code,
      title,
      user_id,
      project_id,
    });
  }

  async update(req, res) {
    const solicitation = await Solicitation.findOne({
      where: { code: req.body.code },
    });

    if (!solicitation) {
      return res.status(400).json({ error: 'Solicitation does not match' });
    }

    const { project_id } = await req.body;

    if (project_id && solicitation.project_id !== null) {
      return res
        .status(401)
        .json({ error: 'Solicitation already has a registered project' });
    }

    const { id, code, title, user_id } = await Solicitation.update(req.body);

    return res.json({
      id,
      code,
      title,
      user_id,
    });
  }

  async index(req, res) {
    const solicitations = await Solicitation.findAll({
      attributes: ['id', 'code', 'title', 'user_id', 'project_id'],
    });

    return res.json(solicitations);
  }
}

export default new SolicitationController();
