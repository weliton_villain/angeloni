import { Router } from 'express';

import UserController from './app/controllers/UserController';
import SessionController from './app/controllers/SessionController';
import ManagementController from './app/controllers/ManagementController';
import AreaController from './app/controllers/AreaController';

import authMiddleware from './app/middlewares/auth';

const routes = new Router();

routes.post('/sessions', SessionController.store);
routes.post('/managements', ManagementController.store);
routes.post('/areas', AreaController.store);

routes.use(authMiddleware);

routes.post('/users', UserController.store);

export default routes;
